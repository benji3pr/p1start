package edu.uprm.cse.datastructures.cardealer.model;

import java.util.Comparator;

public class CarComparator implements Comparator<Car>{

	@Override
	
	// returns positive if c1<c2, negative if c1>c2, 0 if c1==c2
	public int compare(Car c1, Car c2) {
		if(c1==null || c2==null) throw new IllegalArgumentException("Invalid arguments, atleast one is null");
	
		String s1 = c1.getCarBrand()+c1.getCarModel()+c1.getCarModelOption();
		String s2 = c2.getCarBrand()+c2.getCarModel()+c2.getCarModelOption();
		
		if(s1.length()>s2.length()) {
			s1= s1.substring(0, s2.length()-1);
		}
		if(s2.length()>s1.length()) {
			s2= s2.substring(0, s1.length()-1);
		}
		
		s1 = s1.toLowerCase();
		s2 = s2.toLowerCase();
		
		int result = s1.compareTo(s2);
		
		return result;
	}
	

}
