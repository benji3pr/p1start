package edu.uprm.cse.datastructures.cardealer.util;

import java.util.Iterator;

public interface SortedList<E> extends Iterable<E>{

		// valid position in the list
		// [0, size() - 1]
		
		public boolean add(E obj);            //Works
		public int size();                    //Works
		public boolean remove(E obj);		  //Works
		public boolean remove(int index);	  //Works
		public int removeAll(E obj);    	  //Works
		public E first();					  //Works
		public E last();					  //Works
		public E get(int index);              //Works
		public void clear(); 				  //Works
		public boolean contains(E e);         //Works
		public boolean isEmpty();             //Works
		public int firstIndex(E e);  		  //Works
		public int lastIndex(E e);			  //Works

		
}
